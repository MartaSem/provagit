class Funzionalita:
    def __init__(self, contatore: int = 0):

        self.contatore = contatore
        
    def scambiaPosto(self, parola):
        self.contatore = self.contatore + 1
        alfabeto = ["a","b","c","d","e","f","g","h","i","j","k","l","m"]
        alfabeto2 = ["n","o","p","q","r","s","t","u","v","w","x","y","z"] 
        stringafinale = ""
        for i in parola:
            if i in alfabeto:
                stringafinale = stringafinale + alfabeto2[-(alfabeto.index(i) + 1)]
            else:
                stringafinale = stringafinale + alfabeto[-(alfabeto2.index(i) + 1)]
        return stringafinale

    def contaAnimali(self):
        self.contatore = self.contatore + 1
        conigli = 12
        galline = 35 - conigli
        ris = print(f"Le galline sono  {galline}, i conigli sono  {conigli}.")
        return galline, conigli
    
    def contalettere (self, parola, lettera):
        self.contatore = self.contatore + 1
        contatorelettera = 0
        for i in self.scambiaPosto(parola):
            if lettera == i:
                contatorelettera = contatorelettera + 1

        if contatorelettera == 0:
                return print("la lettera non e' presente nella parola")
        else:
            return print (f"la lettera {lettera} e' presente {contatorelettera} volte nella parola modificata")
    
    def contaDigit(self):
        self.contatore = self.contatore + 1
        numGalline = str(self.contaAnimali()[0])
        numConigli = str(self.contaAnimali()[1])
        
        return len(numGalline), len(numConigli)
        

def main():
    funzionalita1 = Funzionalita()
    print(funzionalita1.scambiaPosto("ciao"))
    
    funzionalita1.contaAnimali()
    funzionalita1.contalettere("ciaoo", "l")
    funzionalita1.contalettere("ciao", "y")

    print(funzionalita1.contaAnimali())
    print(funzionalita1.contaDigit())

if __name__ == "__main__":
    main()

