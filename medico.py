class Medico:
    def __init__(self, nome: str, cognome: str, luogonascita :str, annonascita: int, annolaurea: int, sedelavoro: str):

        self.nome = nome
        self.cognome = cognome
        self.luogonascita = luogonascita
        self.annonascita = annonascita
        self.annonlaurea = annolaurea
        self.sedelavoro = sedelavoro

    def modificaSedeLavoro(self, nuovasede):
        self.sedelavoro = nuovasede
    
    def getAnnoNascita(self):
        return self.annonascita
    
    def getNome(self):
        return self.nome

    def verificaAnnoLaurea(self, anno):
        if self.annolaurea <= anno:
            print("il medico si è laureato prima dell'/nell' anno inserito")
            return True
        else:
            print("il medico non si è laureato prima dell'anno inserito")
            return False

    def __eq__ (self, other):
        return self.__dict__ == other.__dict__ #metodo per controllare se due oggetti della stessa classe corrispondono

    def descriviMedico (self):
        return f"Il medico {self.nome} {self.cognome} e' nato a {self.luogonascita}, nel {self.annonascita}. Ha conseguito la laurea nel {self.annonlaurea} e lavora a {self.sedelavoro}"
       
def getGiovane (medico1, medico2):
    if medico1.getAnnoNascita() > medico2.getAnnoNascita():
        return f"E' piu' giovane il medico {medico1.getNome()}"
    elif medico1.getAnnoNascita() == medico2.getAnnoNascita():
        return "sono coetanei"

class MedicoS(Medico):
    def __init__(self, specializzazione: str, annoS: int, durataS :int, nome: str, cognome: str, luogonascita :str, annonascita: int, annolaurea: int, sedelavoro: str):
        super().__init__(nome, cognome, luogonascita, annonascita, annolaurea, sedelavoro)
    
        self.specializzazione = specializzazione
        self.annoS = annoS
        self.durataS = durataS

def main():
    medico1 = Medico("Antonio","Salvi", "Napoli", "1993", "2018", "Treviso")
    medico2 = Medico("Marco","Salvi", "Napoli", "1992", "2018", "Treviso")

    print(medico1 == medico2)
    print(getGiovane(medico1,medico2))
    print(medico1.descriviMedico())

    medicoS1 = MedicoS("pediatria", "2022", "4", "Antonio","Salvi", "Napoli", "1993", "2018", "Treviso")



if __name__ == "__main__":
    main()